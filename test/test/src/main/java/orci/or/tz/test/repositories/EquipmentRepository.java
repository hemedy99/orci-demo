package orci.or.tz.test.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import orci.or.tz.test.models.Equipment;

@Repository
public interface EquipmentRepository extends JpaRepository<Equipment,Integer> {
    

    List<Equipment>findByName(String name);
}
