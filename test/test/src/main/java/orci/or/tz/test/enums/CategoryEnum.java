package orci.or.tz.test.enums;

public enum CategoryEnum {
    ICT,
    FURNITURE,
    ELECTRONICS,
    SECURITY_DEVICES
}
