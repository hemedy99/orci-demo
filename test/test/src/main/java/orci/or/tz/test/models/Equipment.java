package orci.or.tz.test.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import orci.or.tz.test.enums.CategoryEnum;
import orci.or.tz.test.enums.StatusEnum;

@Table(name="equipments")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_seq")
    @SequenceGenerator(name = "id_seq", sequenceName = "ID_SEQ", initialValue = 1, allocationSize = 1)
    @Column(name = "id")
    private Integer id;

    @Column(name="equipment_name")
    private String name;

    @Column(name="serial_number")
    private String serialNumber;

    @Column(name="category")
    @Enumerated(EnumType.STRING)
    private CategoryEnum category;

    
    @Column(name="created_date")
    private LocalDate createdDate;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private StatusEnum status;
    
}
