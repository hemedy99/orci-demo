package orci.or.tz.test.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javassist.NotFoundException;
import orci.or.tz.test.models.Equipment;
import orci.or.tz.test.repositories.EquipmentRepository;

@RestController
public class EquipmentController {

@Autowired
private EquipmentRepository equipmentRepository;




@GetMapping(path = "/api/equipment", produces = MediaType.APPLICATION_XML_VALUE)
public ResponseEntity<List<Equipment>> GetAllEquipments(){
    List<Equipment> equipments = equipmentRepository.findAll();
    return ResponseEntity.ok(equipments);
}

@GetMapping(path = "/api/equipment/{id}", produces = MediaType.APPLICATION_XML_VALUE)
public ResponseEntity<?> GetAllEquipmentsById(@PathVariable Integer id) throws NotFoundException
{
    Optional<Equipment> equipments = equipmentRepository.findById(id);

    if(equipments.isPresent()){
return ResponseEntity.ok(equipments.get());
    }else{
 throw new NotFoundException("Equipment Not found");
    }
    
}

}
